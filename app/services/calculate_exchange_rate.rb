class CalculateExchangeRate
  def self.call(args)
    new(**args).call
  end

  def initialize(from_currency:, to_currency:, amount:)
    @from_currency = from_currency
    @to_currency = to_currency
    @amount = amount
  end

  def call
    rate = exchange_rates[from_currency.to_sym][to_currency.to_sym]
    { exchange_rate: rate, amount: rate * amount.to_i }
  end

  private

  attr_reader :from_currency, :to_currency, :amount

  def exchange_rates
    {
      'GBP': {
        'USD': 1.28,
        'GBP': 1,
        'EUR': 1.16
      },
      'USD': {
        'USD': 1,
        'GBP': 0.78,
        'EUR': 0.91
      },
      'EUR': {
        'USD': 1.1,
        'GBP': 0.86,
        'EUR': 1
      }
    }
  end
end
