class CalculateBalances
  def self.call(args)
    new(**args).call
  end

  def initialize(user:)
    @user = user
  end

  def call
    raise ArgumentError, 'Invalid User' if user.blank?

    transactions = Transaction.where(sender: user).or(Transaction.where(receiver: user))

    build_json(calculate(transactions))
  end

  private

  attr_reader :user

  def calculate(transactions)
    currencies = { gbp: 0, eur: 0, usd: 0 }.with_indifferent_access
    transactions.each { |transaction| add_transaction(currencies, transaction) }

    return currencies
  end

  def add_transaction(currencies, transaction)
    if(transaction.sender_id == user.id)
      currencies[transaction.sender_currency.downcase] -= transaction.sender_amount
    elsif (transaction.receiver_id == user.id)
      currencies[transaction.receiver_currency.downcase] += transaction.receiver_amount
    else
      raise "We got a transaction we shouldn't have"
    end
  end

  def build_json(currencies)
    [
      { currency: "GBP", amount: currencies[:gbp] },
      { currency: "EUR", amount: currencies[:eur] },
      { currency: "USD", amount: currencies[:usd] },
    ]
  end
end
