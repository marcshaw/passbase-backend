class CreateTransaction
  def self.call(args)
    new(**args).call
  end

  def initialize(user:, sender_currency:, receiver_currency:, sender_amount:, receiver:)
    @user = user
    @sender_currency = sender_currency
    @receiver_currency = receiver_currency
    @sender_amount = sender_amount
    @receiver = receiver
  end

  def call
    return { txn: nil, errors: validations } if validations.present?

    txn = Transaction.create!(
      sender: user,
      receiver: User.find(receiver),
      sender_currency: sender_currency,
      receiver_currency: receiver_currency,
      sender_amount: sender_amount,
      receiver_amount: exchange_info[:amount],
      exchange_rate: exchange_info[:exchange_rate]
    )

    { txn: txn, errors: [] }
  end

  private

  attr_reader :user, :sender_currency, :receiver_currency, :sender_amount, :receiver

  def exchange_info
    @exchange_info ||= CalculateExchangeRate.call(
      from_currency: sender_currency,
      to_currency: receiver_currency,
      amount: sender_amount
    )
  end

  def validations
    @validations ||= ValidateTransaction.call(
      user: user,
      sender_currency: sender_currency,
      receiver_currency: receiver_currency,
      sender_amount: sender_amount,
      receiver: receiver
    )
  end
end
