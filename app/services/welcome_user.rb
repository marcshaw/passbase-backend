class WelcomeUser
  def self.call(args)
    new(**args).call
  end

  def initialize(user:)
    @user = user
  end

  def call
    raise ArgumentError, 'Invalid User' if user.blank?

    # TODO Should we check there are no existing transactions from welcome to this user?

    Transaction.create!(
      sender: User.find_by(email: 'welcome@pb.com'),
      receiver: user,
      sender_currency: 'USD',
      receiver_currency: 'USD',
      sender_amount: 1000,
      receiver_amount: 1000,
      exchange_rate: '1'
    )
  end

  private

  attr_reader :user
end
