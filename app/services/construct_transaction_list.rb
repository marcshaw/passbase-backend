class ConstructTransactionList
  def self.call(args)
    new(**args).call
  end

  def initialize(user:)
    @user = user
  end

  def call
    raise ArgumentError, 'Invalid User' if user.blank?

    transactions = user.transactions.includes(:sender)

    transactions.map do |txn|
      txn.attributes.merge(
        new_attributes(txn)
      )
    end
  end

  private

  attr_reader :user

  def new_attributes(txn)
    if(txn.sender_id == user.id)
      { sender_name: user.name, receiver_name: txn.receiver.name }
    elsif (txn.receiver_id == user.id)
      { sender_name: txn.sender.name, receiver_name: user.name }
    else
      raise "We got a transaction we shouldn't have"
    end
  end
end
