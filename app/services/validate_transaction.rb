class ValidateTransaction
  def self.call(args)
    new(**args).call
  end

  def initialize(user:, sender_currency:, receiver_currency:, sender_amount:, receiver:)
    @user = user
    @sender_currency = sender_currency
    @receiver_currency = receiver_currency
    @sender_amount = sender_amount
    @receiver = receiver
  end

  def call
    return 'Invalid Sender Currency' if invalid_currency_type(sender_currency)
    return 'Invalid Receiver Currency' if invalid_currency_type(receiver_currency)
    return 'Invalid Balance' if invalid_balance
    return 'Invalid Sender Amount' if invalid_sender_amount
  end

  private

  attr_reader :user, :sender_currency, :receiver_currency, :sender_amount, :receiver

  def invalid_sender_amount
    sender_amount.to_i <= 0
  end

  def invalid_currency_type(currency)
    Currency::CURRENCIES.exclude?(currency)
  end

  def invalid_balance
    sender_amount.to_i > balance
  end

  def balance
    balances = CalculateBalances.call(user: user)
    balances.find { |hash| hash[:currency] == sender_currency }[:amount]
  end
end
