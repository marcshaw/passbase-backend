class TransactionsController < ApplicationController
  before_action :authenticate_user!

  def index
    render json: ConstructTransactionList.call(user: current_user), status: 200
  end

  def create
    result = CreateTransaction.call(create_params)

    if result[:errors].present?
      render json: result, status: 400
    else
      render json: result, status: 201
    end
  end

  private

  def create_params
    permitted_params = params
      .require(:transaction)
      .permit(:sender_currency, :receiver_currency, :sender_amount, :receiver)

    permitted_params.to_h.symbolize_keys.merge(user: current_user)
  end
end
