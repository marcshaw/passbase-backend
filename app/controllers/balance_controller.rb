class BalanceController < ApplicationController
  before_action :authenticate_user!

  def index
    render json: CalculateBalances.call(user: current_user), status: 200
  end
end
