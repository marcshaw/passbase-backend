class RegistrationsController < Devise::RegistrationsController
  respond_to :json
  before_action :configure_permitted_parameters

  def create
    build_resource(sign_up_params)

    if resource.save
      WelcomeUser.call(user: resource)
      render json: resource, status: :created
    else
      render json: error_response(resource), status: :bad_request
    end
  end

  private

  def error_response(resource)
    resource.errors
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up) { |u| u.permit(:name, :email, :password)}
  end
end
