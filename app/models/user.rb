class User < ApplicationRecord
  devise :database_authenticatable,
         :jwt_authenticatable,
         :registerable,
         jwt_revocation_strategy: JWTBlacklist

  validates :email, uniqueness: true

  def transactions
    Transaction.where(sender: self).or(Transaction.where(receiver: self))
  end
end
