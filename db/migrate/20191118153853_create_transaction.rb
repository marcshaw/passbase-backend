class CreateTransaction < ActiveRecord::Migration[5.2]
  def change
    create_table :transactions do |t|
      t.belongs_to :sender, class_name: 'User', null: false
      t.belongs_to :receiver, class_name: 'User', null: false
      t.decimal :sender_amount, null: false
      t.decimal :receiver_amount, null: false
      t.decimal :exchange_rate, null: false
      t.string :sender_currency, null: false
      t.string :receiver_currency, null: false

      t.timestamps
    end
  end
end
