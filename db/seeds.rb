# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

welcome_user = User.create!(name: "Welcome", email: 'welcome@pb.com', password: 'asefasdfq34r324fasdfasdfasdfESA@##33323@@@@22!!!')
jim = User.create!(name: "Jim", email: 'jimmy@jim.com', password: 'jimjim')
WelcomeUser.call(user: jim)
marc = User.create!(name: "Marc", email: 'marc@marc.com', password: 'marcmarc')
WelcomeUser.call(user: marc)
