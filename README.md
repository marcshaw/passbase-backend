## TODO

* Save failed transactions
* Create endpoint to return all the users

## To Run

* docker-compose build
* docker-compose run web rake db:create db:migrate db:seed
* docker-compose up

Login info: jimmy@jim.com / jimjim


It will expose port 3001.
