require 'rails_helper'

describe User, type: :model do
  context '#transactions' do
    subject(:transactions) { user.transactions }

    let(:user) { create(:user) }
    let(:other_user) { create(:user) }
    let(:another_user) { create(:user) }
    let!(:transaction_1) { create(:transaction, sender: user, receiver: another_user) }
    let!(:transaction_2) { create(:transaction, sender: other_user, receiver: user) }
    let!(:transaction_3) { create(:transaction, sender: other_user, receiver: another_user) }

    it 'returns both sent and received transactions' do
      expect(transactions).to contain_exactly(transaction_1, transaction_2)
    end
  end

  context '#balance' do
    subject(:balance) { user.balance }

    let(:user) { create(:user) }
    let(:other_user) { create(:user) }

    context '#validations' do
      context 'unique email' do
        let(:old_user) { create(:user) }

        it 'has an error on the object' do
          new_user = build(:user, email: old_user.email)

          expect(new_user.save).to eq(false)
          expect(new_user.errors[:email]).to include(/taken/)
        end
      end
    end
  end
end
