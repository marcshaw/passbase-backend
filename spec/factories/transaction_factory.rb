FactoryBot.define do
  factory :transaction do
    sender { create(:user) }
    receiver { create(:user) }
    sender_amount { 1000 }
    receiver_amount { 1000 }
    receiver_currency { Currency::USD }
    sender_currency { Currency::USD }
    exchange_rate { 1 }
  end
end
