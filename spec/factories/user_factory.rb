FactoryBot.define do
  factory :user do
    name { "test" }
    password { "password" }

    sequence :email do |n|
      "test#{n}@example.com"
    end
  end
end
