require 'rails_helper'

describe WelcomeUser do
  subject(:welcome_user) { described_class.call(user: user) }
  let!(:welcome_object) { create(:user, email: 'welcome@pb.com') }

  context 'when given a valid user' do
    let(:user) { create(:user) }

    it 'creates a transaction of 1000 to the user' do
      expect { welcome_user }.to change { user.transactions.count }
        .from(0)
        .to(1)

      expect(user.transactions.first.receiver_amount).to eq(1000)
    end

    it 'creates the transaction from the welcome user' do
      expect { welcome_user }.to change { welcome_object.transactions.count }
        .from(0)
        .to(1)

      expect(welcome_object.transactions.first.sender_amount).to eq(1000)
    end
  end

  context 'when given an invalid user' do
    let(:user) { nil }

    it 'raises an error' do
      expect { welcome_user } .to raise_error(ArgumentError, /Invalid User/)
    end
  end
end
