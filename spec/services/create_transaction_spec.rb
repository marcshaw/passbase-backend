require 'rails_helper'

describe CreateTransaction do
  subject(:create_txn) { described_class.call(params) }
  let(:user) { create(:user) }
  let(:receiver) { create(:user) }
  let(:sender_currency) { "USD" }
  let(:receiver_currency) { "EUR" }
  let(:sender_amount) { 10 }

  let(:params) do
    {
      user: user,
      receiver: receiver.id,
      sender_currency: sender_currency,
      receiver_currency: receiver_currency,
      sender_amount: sender_amount
    }
  end

  let(:validate_result) { nil }

  before do
    allow(ValidateTransaction).to receive(:call).and_return(validate_result)
  end


  context 'when the params are valid' do
    it 'creates a transaction and returns it' do
      expect { create_txn }.to change { Transaction.count }.from(0).to(1)
      expect(create_txn).to eq({ txn: Transaction.last, errors: [] })
      expect(create_txn[:txn].attributes.symbolize_keys).to include({
        exchange_rate: 0.91e0,
        receiver_amount: 9.1,
        receiver_currency: receiver_currency,
        receiver_id: receiver.id,
        sender_amount: sender_amount,
        sender_currency: sender_currency,
        sender_id: user.id
      })
    end
  end

  context 'when the params are invalid' do
    let(:validate_result) { 'Invalid' }

    it 'calls the validate transaction service' do
      expect(ValidateTransaction).to receive(:call).with(
        {
          user: user,
          sender_currency: sender_currency,
          receiver_currency: receiver_currency,
          sender_amount: sender_amount,
          receiver: receiver.id
        })

      create_txn
    end

    it 'returns the error' do
      expect(create_txn).to eq({txn: nil, errors: validate_result})
    end
  end
end
