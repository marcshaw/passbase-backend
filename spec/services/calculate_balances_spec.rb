require 'rails_helper'

describe CalculateBalances do
  subject(:calculate_balance) { described_class.call(user: user) }

  context 'when given a valid user' do
    let(:user) { create(:user) }
    let(:expected_result) do
      [
        { currency: "GBP", amount: 100 },
        { currency: "EUR", amount: 0 },
        { currency: "USD", amount: 50 },
      ]
    end

    before do
      create(:transaction, receiver: user, receiver_amount: 100, receiver_currency: "GBP")
      create(:transaction, receiver: user, receiver_amount: 100, receiver_currency: "USD")
      create(:transaction, sender: user, sender_amount: 50, sender_currency: "USD")
    end

    it 'returns the expected balances' do
      expect(calculate_balance).to eq(expected_result)
    end
  end

  context 'when given an invalid user' do
    let(:user) { nil }

    it 'raises an error' do
      expect { calculate_balance } .to raise_error(ArgumentError, /Invalid User/)
    end
  end
end
