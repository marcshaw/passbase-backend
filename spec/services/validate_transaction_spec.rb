require 'rails_helper'

describe ValidateTransaction do
  subject(:validate_transaction) { described_class.call(params) }
  let(:user) { create(:user) }
  let(:receiver) { create(:user) }
  let(:sender_currency) { "USD" }
  let(:receiver_currency) { "EUR" }
  let(:sender_amount) { 10 }

  let(:params) do
    {
      user: user,
      sender_currency: sender_currency,
      receiver_currency: receiver_currency,
      sender_amount: sender_amount,
      receiver: receiver.id
    }
  end

  context 'when vailid' do
    before do
      create(:transaction, receiver: user, receiver_currency: sender_currency)
    end

    it 'returns nil' do
      expect(validate_transaction).to eq(nil)
    end
  end

  context 'when send currency is invalid' do
    let(:sender_currency) { "not" }

    it 'returns sender currency error' do
      expect(validate_transaction).to eq('Invalid Sender Currency')
    end
  end

  context 'when recevier currency is invalid' do
    let(:receiver_currency) { "not" }

    it 'returns receiver currency error' do
      expect(validate_transaction).to eq('Invalid Receiver Currency')
    end
  end

  context 'when amount is invalid' do
    let(:sender_amount) { 0 }

    it 'returns sender amount error' do
      expect(validate_transaction).to eq('Invalid Sender Amount')
    end
  end

  context 'when balance is invalid' do
    it 'returns balance error' do
      expect(validate_transaction).to eq('Invalid Balance')
    end
  end
end
