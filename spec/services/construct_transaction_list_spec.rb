require 'rails_helper'

describe ConstructTransactionList do
  subject(:construct_list) { described_class.call(user: user) }

  context 'when given a valid user' do
    let(:user) { create(:user) }
    let(:other_user) { create(:user) }
    let!(:txn1) { create(:transaction, receiver: user, sender: other_user, receiver_amount: 100, receiver_currency: 'GBP') }
    let!(:txn2) { create(:transaction, receiver: other_user, sender: user, sender_amount: 100, receiver_currency: 'USD') }

    let(:expected_result) do
      [
        txn1.attributes.merge(receiver_name: user.name, sender_name: other_user.name),
        txn2.attributes.merge(sender_name: user.name, receiver_name: other_user.name),
      ]
    end

    it 'returns the expected transactions' do
      expect(construct_list).to eq(expected_result)
    end
  end

  context 'when given an invalid user' do
    let(:user) { nil }

    it 'raises an error' do
      expect { construct_list } .to raise_error(ArgumentError, /Invalid User/)
    end
  end
end
