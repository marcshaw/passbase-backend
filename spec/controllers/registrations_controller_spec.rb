require 'rails_helper'

describe RegistrationsController, type: :request do
  describe 'POST /signup' do
    let(:url) { '/signup' }
    let(:params) do
      {
        user: {
          email: 'user@example.com',
          password: 'password',
          name: 'user',
        }
      }
    end
    let(:parsed_body) { JSON.parse(response.body) }

    before do
      allow(WelcomeUser).to receive(:call)
    end

    context 'when user is unauthenticated' do
      before { post url, params: params }

      it 'returns 201' do
        expect(response.status).to eq 201
      end

      it 'returns a new user' do
        expect(parsed_body).to include('email' => 'user@example.com')
      end

      it 'calls the WelcomeUser service' do
        expect(WelcomeUser).to have_received(:call)
      end
    end

    context 'when user already exists' do
      before do
        create(:user, email: params[:user][:email])
        post url, params: params
      end

      it 'returns bad request status' do
        expect(response.status).to eq 400
      end

      it 'returns validation errors' do
        expect(parsed_body).to include('email' => [/already been taken/])
      end
    end
  end
end
