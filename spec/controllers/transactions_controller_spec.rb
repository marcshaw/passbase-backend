require 'rails_helper'
require 'devise/jwt/test_helpers'

describe TransactionsController, type: :request do
  let(:parsed_body) { JSON.parse(response.body) }

  describe 'GET /transactions' do
    subject(:transactions) { get url, headers: auth_header }

    let(:url) { '/transactions' }
    let(:user) { create(:user) }
    let(:headers) { { 'Accept' => 'application/json', 'Content-Type' => 'application/json' } }
    let(:auth_header) { Devise::JWT::TestHelpers.auth_headers(headers, user) }

    let!(:transaction_1) { create(:transaction, sender: user) }
    let!(:transaction_2) { create(:transaction, receiver: user) }

    before do
      allow(ConstructTransactionList).to receive(:call)
    end

    it 'calls the construct transaction service' do
      expect(ConstructTransactionList).to receive(:call).with(user: user)

      transactions
    end

    it 'returns a 200' do
      transactions
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'CREATE /transactions' do
    subject(:create_transaction) { post url, headers: auth_header, params: params.to_json }

    let(:url) { '/transactions' }
    let(:user) { create(:user) }
    let(:headers) { { 'Accept' => 'application/json', 'Content-Type' => 'application/json' } }
    let(:auth_header) { Devise::JWT::TestHelpers.auth_headers(headers, user) }
    let(:params) { { transaction: { sender_currency: '1', receiver_currency: '2', sender_amount: '3', receiver: '4' } } }
    let(:service_result) { {} }

    before do
      allow(CreateTransaction).to receive(:call).and_return(service_result)
    end

    it 'calls the create transaction service' do
      expect(CreateTransaction).to receive(:call).with(params[:transaction].merge(user: user))
      create_transaction
    end

    context 'when a transaction is successfully created' do
      it 'returns with no error and status code' do
        create_transaction

        expect(response).to have_http_status(:created)
        expect(response.body).to eq(service_result.to_json)
      end
    end

    context 'when there is an error' do
      let(:service_result) { { errors: 'invalid' } }

      it 'returns the error and status code' do
        create_transaction

        expect(response).to have_http_status(:bad_request)
        expect(response.body).to eq(service_result.to_json)
      end
    end
  end
end
