require 'rails_helper'
require 'devise/jwt/test_helpers'

describe BalanceController, type: :request do
  let(:parsed_body) { JSON.parse(response.body) }

  describe 'GET /balance' do
    subject(:balance) { get url, headers: auth_header }

    let(:url) { '/balance' }
    let(:user) { create(:user) }
    let(:headers) { { 'Accept' => 'application/json', 'Content-Type' => 'application/json' } }
    let(:auth_header) { Devise::JWT::TestHelpers.auth_headers(headers, user) }

    it 'calls the calculate balance service' do
      expect(CalculateBalances).to receive(:call).with(user: user)

      balance
    end
  end
end
